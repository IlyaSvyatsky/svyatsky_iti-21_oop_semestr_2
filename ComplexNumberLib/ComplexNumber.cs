﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComplexNumberLib
{

    /// <summary>
    /// ComplexNumber - класс для комплексного числа
    /// </summary>
    public class ComplexNumber
    {
        //комплексное число - a+b*i, где a,b - действительные части,а i - мнимая
        public double a;
        public double b;
        
        /// <summary>
        /// Запись в поля объекта,где a,b - действительные части
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        public ComplexNumber(double a = 0, double b = 0)
        {
            this.a = a;
            this.b = b;
        }

        /// <summary>
        /// Получение части a
        /// </summary>
        public double A
        {
            get { return a; }
        }

        /// <summary>
        /// Получение части b
        /// </summary>
        public double B
        {
            get { return b; }
        }

        /// <summary>
        /// GetNumber - получение строки с значением комплексного числа
        /// </summary>
        public string GetNumber
        {
            get
            {
                string result = "";
                if (b > 0)
                {
                    result = Convert.ToString(a) + "+" + Convert.ToString(b) + "*i";
                }
                else
                {
                    result = Convert.ToString(a) + Convert.ToString(b) + "*i";                   
                }
                return result;
            }
        }
        
        /// <summary>
        /// Перегрузка оператора + 
        /// </summary>
        /// <param name="number1"></param>
        /// <param name="number2"></param>
        /// <returns></returns>
        /// //i*i = -1; 
        public static ComplexNumber operator +(ComplexNumber number1, ComplexNumber number2)
        {
            //(a+b*i)(a1+b1*i) = a*a1 + a*b1*i + a1*b*i - b*b1 = a*a1 - b*b1 +(a*b1 + a1*b)*i = a2 + b2*i
            return new ComplexNumber(number1.A + number2.A, number1.B + number2.B);
        }

        //a2 = a*a1 - b*b1
        //b2 = a*b1 + a1*b

        /// <summary>
        /// Перегрузка оператора * 
        /// </summary>
        /// <param name="number1"></param>
        /// <param name="number2"></param>
        /// <returns></returns>
        public static ComplexNumber operator *(ComplexNumber number1, ComplexNumber number2)
        {
            return new ComplexNumber(number1.A * number2.A - number1.B * number2.B, number1.A * number2.B + number2.A * number1.B);
        }
    }
}
