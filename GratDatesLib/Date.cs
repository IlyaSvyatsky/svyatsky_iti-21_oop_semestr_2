﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreatDatesLib
{
    public class Date
    {
        public Int32 DateId { get; set; }

        public DateTime EventDate { get; set; }

        //public Int32 EventNameId { get; set; }
        public Int32 CountryId { get; set; }

        //public String Country { get; set; }
        public String EventName { get; set; }

        public Int32 EventTypeId { get; set; }

        public String VictimsNumber { get; set; }

        public Date(Int32 dateId, DateTime eventDate, String eventName, Int32 countryId, Int32 eventTypeId, String victimsNumber)
        {
            DateId = dateId;
            EventDate = eventDate;
            CountryId = countryId;
            EventName = eventName;
            EventTypeId = eventTypeId;
            VictimsNumber = victimsNumber;
        }

        public override string ToString()
        {
            return String.Format("{0,-5} {1,-20} {2,-30} {3, -15} {4,-15} {5, -15}",
                DateId,
                EventDate.ToString(@"dd\.MM\.yyyy"),
                EventName,
                MSSQCountry.GetCountryByIndex(CountryId),
                MSSQEventType.GetEventTypeByIndex(EventTypeId),
                VictimsNumber);
        }

        public override bool Equals(object obj)
        {
            {
                return obj is Date date &&
                       EventDate == date.EventDate &&
                       CountryId == date.CountryId &&
                       EventName == date.EventName &&
                       EventTypeId == date.EventTypeId &&
                       VictimsNumber == date.VictimsNumber &&
                       DateId == DateId;
            }
        }

        public override int GetHashCode()
        {
            int hashCode = -210507647;
            hashCode = hashCode * -1521134295 + EqualityComparer<DateTime>.Default.GetHashCode(EventDate);
            //hashCode = hashCode * -1521134295 + EventDate.GetHashCode();
            hashCode = hashCode * -1521134295 + EventName.GetHashCode();
            hashCode = hashCode * -1521134295 + CountryId.GetHashCode();
            hashCode = hashCode * -1521134295 + EventTypeId.GetHashCode();
            hashCode = hashCode * -1521134295 + VictimsNumber.GetHashCode();
            hashCode = hashCode * -1521134295 + DateId.GetHashCode();
            return hashCode;
        }
    }
}
