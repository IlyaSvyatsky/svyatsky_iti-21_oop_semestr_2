﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreatDatesLib
{
    public static class MSSQCountry
    {
        private const string GET_COUNTRIES_EXPRESSION
            = "SELECT * FROM Countries";

        private const string GET_COUNTRY_EXPRESSION
            = "SELECT * FROM Countries WHERE CountryId={0}";

        private const string GET_COUNTRY_ID_EXPRESSION
            = "SELECT CountryId FROM Countries WHERE CountryName='{0}'";

        private static string connectString;

        public static void Initialization(string connectString)
        {
            MSSQCountry.connectString = connectString;
        }

        public static string GetCountryByIndex(int id)
        {
            string eventName = "";
            using (SqlConnection connection = new SqlConnection(connectString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(
                    string.Format(GET_COUNTRY_EXPRESSION, id),
                    connection);

                SqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    reader.Read();
                    eventName = reader.GetString(1);
                }
            }
            return eventName;
        }

        public static int GetIndexByName(string name)
        {
            int id = 0;
            using (SqlConnection sqlConnection = new SqlConnection(connectString))
            {
                sqlConnection.Open();
                SqlCommand sqlCommand = new SqlCommand(
                    string.Format(GET_COUNTRY_ID_EXPRESSION,
                    name), sqlConnection);
                SqlDataReader reader = sqlCommand.ExecuteReader();
                if (reader.HasRows)
                {
                    reader.Read();
                    id = reader.GetInt32(0);
                }
            }
            return id;
        }

        public static string[] GetCountries()
        {
            List<string> eventNames = new List<string>();
            using (SqlConnection connection = new SqlConnection(connectString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(
                   string.Format(GET_COUNTRIES_EXPRESSION),
                    connection);

                SqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows)
                    while (reader.Read())
                        eventNames.Add(reader.GetString(1));
            }
            return eventNames.ToArray();
        }
    }
}
