﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreatDatesLib
{
    public static class MSSQDate
    {
        private const string GET_DATE_ID_EXPRESSION
            = "SELECT DateId FROM Dates";

        private const string GET_DATE_INFO_EXPRESSION
            = "SELECT * FROM Dates";

        private const string GET_DATE_BY_INDEX_EXPRESSION
            = "SELECT * FROM Dates WHERE DateId={0}";

        private const string INSERT_DATE_EXPRESSION
            = "INSERT INTO Dates(EventDate,  EventName, CountryId, EventTypeId, VictimsNumber) VALUES (@eventDate, @eventName, @countryId, @eventTypeId, @victimsNumber)";

        private const string DELETE_DATE_EXPRESSION
            = "DELETE FROM Dates WHERE DateId={0}";

        private const string UPDATE_DATE_EXPRESSION
            = "UPDATE Dates SET EventDate = @eventDate, EventName = @eventName, CountryId = @countryId, EventTypeId = @eventTypeId, VictimsNumber = @victimsNumber WHERE DateId = @dateId";

        private static string connectString;
        public static void Initialization(string connectString)
        {
            MSSQDate.connectString = connectString;
        }

        public static bool Delete(Date date)
        {
            int numb;
            using (SqlConnection sqlConnection = new SqlConnection(connectString))
            {
                int id = date.DateId;

                sqlConnection.Open();
                SqlCommand sqlCommand = new SqlCommand(
                    string.Format(DELETE_DATE_EXPRESSION,
                    id), sqlConnection);
                numb = sqlCommand.ExecuteNonQuery();
            }
            return numb > 0;
        }

        public static int[] GetFirsIds()
        {
            List<int> id = new List<int>();
            using (SqlConnection sqlConnection = new SqlConnection(connectString))
            {
                sqlConnection.Open();
                SqlCommand sqlCommand = new SqlCommand(
                    string.Format(GET_DATE_ID_EXPRESSION), sqlConnection);
                SqlDataReader reader = sqlCommand.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                        id.Add(reader.GetInt32(0));
                }
            }
            return id.ToArray();
        }
 
        public static Date GetMusicByIndex(int id)
        {
            Date date = null;
            using (SqlConnection connection = new SqlConnection(connectString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(
                    string.Format(GET_DATE_BY_INDEX_EXPRESSION, id),
                    connection);

                SqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    reader.Read();
                    date = new Date(
                        reader.GetInt32(0),
                        reader.GetDateTime(1),
                        reader.GetString(2),
                        reader.GetInt32(3),
                        reader.GetInt32(4),
                        reader.GetString(5)
                        //reader.GetInt32(5)
                        );
                }
            }
            return date;
        }

        public static string[] GetMusicInfo()
        {
            List<string> dateInfo = new List<string>();
            using (SqlConnection connection = new SqlConnection(connectString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(
                   string.Format(GET_DATE_INFO_EXPRESSION),
                    connection);

                SqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows)
                    while (reader.Read())
                    {
                        dateInfo.Add(new Date(
                        reader.GetInt32(0),
                        reader.GetDateTime(1),
                        reader.GetString(2),
                        reader.GetInt32(3),
                        reader.GetInt32(4),
                        reader.GetString(5)
                        //reader.GetInt32(5)
                        ).ToString());
                    }
            }
            return dateInfo.ToArray();
        }

        public static bool Insert(Date date)
        {
            int numb;
            using (SqlConnection sqlConnection = new SqlConnection(connectString))
            {
                int countryId = date.CountryId;
                int eventTypeId = date.EventTypeId;

                sqlConnection.Open();
                SqlCommand sqlCommand = new SqlCommand(INSERT_DATE_EXPRESSION, sqlConnection);
                sqlCommand.Parameters.Add(new SqlParameter("@eventDate", date.EventDate));
                sqlCommand.Parameters.Add(new SqlParameter("@eventName", date.EventName));
                sqlCommand.Parameters.Add(new SqlParameter("@countryId", countryId));
                sqlCommand.Parameters.Add(new SqlParameter("@eventTypeId", eventTypeId));
                sqlCommand.Parameters.Add(new SqlParameter("@victimsNumber", date.VictimsNumber));
                numb = sqlCommand.ExecuteNonQuery();
            }
            return numb > 0;
        }

        public static bool Update(Date date)
        {
            int numb;

            using (SqlConnection sqlConnection = new SqlConnection(connectString))
            {
                int id = date.DateId;
                int countryId = date.CountryId;
                int eventTypeId = date.EventTypeId;

                sqlConnection.Open();
                SqlCommand sqlCommand = new SqlCommand(UPDATE_DATE_EXPRESSION, sqlConnection);
                sqlCommand.Parameters.Add(new SqlParameter("@dateId", id));
                sqlCommand.Parameters.Add(new SqlParameter("@eventDate", date.EventDate.ToString(@"yyyy\-MM\-dd")));
                sqlCommand.Parameters.Add(new SqlParameter("@eventName", date.EventName));
                sqlCommand.Parameters.Add(new SqlParameter("@countryId", countryId));
                sqlCommand.Parameters.Add(new SqlParameter("@eventTypeId", eventTypeId));
                sqlCommand.Parameters.Add(new SqlParameter("@victimsNumber", date.VictimsNumber));
                numb = sqlCommand.ExecuteNonQuery();
            }
            return numb > 0;
        }
    }
}
