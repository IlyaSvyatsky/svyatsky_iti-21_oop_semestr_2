﻿/*using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreatDatesLib
{
    public static class MSSQEventName
    {
        private const string GET_EVENTNAMES_EXPRESSION
            = "SELECT * FROM EventNames";

        private const string GET_EVENTNAME_EXPRESSION
            = "SELECT * FROM EventNames WHERE EventNameId={0}";

        private const string GET_EVENTNAME_ID_EXPRESSION
            = "SELECT EventNameId FROM EventNames WHERE EventName='{0}'";

        private static string connectString;

        public static void Initialization(string connectString)
        {
            MSSQEventName.connectString = connectString;
        }

        public static string GetEventNameByIndex(int id)
        {
            string eventName = "";
            using (SqlConnection connection = new SqlConnection(connectString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(
                    string.Format(GET_EVENTNAME_EXPRESSION, id),
                    connection);

                SqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    reader.Read();
                    eventName = reader.GetString(1);
                }
            }
            return eventName;
        }

        public static int GetIndexByName(string name)
        {
            int id = 0;
            using (SqlConnection sqlConnection = new SqlConnection(connectString))
            {
                sqlConnection.Open();
                SqlCommand sqlCommand = new SqlCommand(
                    string.Format(GET_EVENTNAME_ID_EXPRESSION,
                    name), sqlConnection);
                SqlDataReader reader = sqlCommand.ExecuteReader();
                if (reader.HasRows)
                {
                    reader.Read();
                    id = reader.GetInt32(0);
                }
            }
            return id;
        }

        public static string[] GetEventNames()
        {
            List<string> eventNames = new List<string>();
            using (SqlConnection connection = new SqlConnection(connectString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(
                   string.Format(GET_EVENTNAMES_EXPRESSION),
                    connection);

                SqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows)
                    while (reader.Read())
                        eventNames.Add(reader.GetString(1));
            }
            return eventNames.ToArray();
        }
    }
}*/
