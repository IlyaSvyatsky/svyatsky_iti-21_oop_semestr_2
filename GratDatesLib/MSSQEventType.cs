﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreatDatesLib
{
    public static class MSSQEventType
    {
        private const string GET_EVENTTYPES_EXPRESSION
            = "SELECT * FROM EventTypes";

        private const string GET_EVENTTYPE_EXPRESSION
            = "SELECT * FROM EventTypes WHERE EventTypeId={0}";

        private const string GET_EVENTTYPE_ID_EXPRESSION
            = "SELECT EventTypeId FROM EventTypes WHERE EventTypeName='{0}'";

        private static string connectString;

        public static void Initialization(string connectString)
        {
            MSSQEventType.connectString = connectString;
        }

        public static string GetEventTypeByIndex(int id)
        {
            string eventType = "";
            using (SqlConnection connection = new SqlConnection(connectString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(
                    string.Format(GET_EVENTTYPE_EXPRESSION, id),
                    connection);

                SqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    reader.Read();
                    eventType = reader.GetString(1);
                }
            }
            return eventType;
        }

        public static int GetIndexByName(string name)
        {
            int id = 0;
            using (SqlConnection sqlConnection = new SqlConnection(connectString))
            {
                sqlConnection.Open();
                SqlCommand sqlCommand = new SqlCommand(
                    string.Format(GET_EVENTTYPE_ID_EXPRESSION,
                    name), sqlConnection);
                SqlDataReader reader = sqlCommand.ExecuteReader();
                if (reader.HasRows)
                {
                    reader.Read();
                    id = reader.GetInt32(0);
                }
            }
            return id;
        }

        public static string[] GetEventTypes()
        {
            List<string> eventTypes = new List<string>();
            using (SqlConnection connection = new SqlConnection(connectString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(
                   string.Format(GET_EVENTTYPES_EXPRESSION),
                    connection);

                SqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows)
                    while (reader.Read())
                        eventTypes.Add(reader.GetString(1));
            }
            return eventTypes.ToArray();
        }
    }
}
