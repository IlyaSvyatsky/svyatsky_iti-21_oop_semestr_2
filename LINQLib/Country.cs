﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Linq.Mapping;

namespace LINQLib
{
    [Table(Name = "Countries")]
    public class Country : IComparable<Country>
    {
        [Column(Name = "CountryId", IsPrimaryKey = true, IsDbGenerated = true)]
        public Int32 CountryId { get; set; }
        [Column(Name = "CountryName")]
        public String CountryName { get; set; }

        public int CompareTo(Country other)
        {
            return CountryId.CompareTo(other.CountryId);
        }

        public override bool Equals(object obj)
        {
            return obj is Country country &&
                CountryId == country.CountryId &&
                CountryName == country.CountryName;
        }

        /// <summary>
        /// хеш
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            int hashCode = -994189688;
            hashCode = hashCode * -1521134295 + CountryId.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(CountryName);
            return hashCode;
        }
    }
}
