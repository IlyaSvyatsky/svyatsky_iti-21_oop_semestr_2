﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Linq.Mapping;
using System.Data.SqlTypes;

namespace LINQLib
{
    [Table(Name = "Dates")]
    public class Date : IComparable<Date>
    {
        [Column(Name = "DateId", IsPrimaryKey = true, IsDbGenerated = true)]
        public int DateId { get; set; }

        [Column(Name = "EventDate")]
        public DateTime EventDate { get; set; }

        [Column(Name = "EventName")]
        public string EventName { get; set; }

        [Column(Name = "CountryId")]
        public int CountryId { get; set; }

        [Column(Name = "EventTypeId")]
        public int EventTypeId { get; set; }

        [Column(Name = "VictimsNumber")]
        public string VictimsNumber { get; set; }

        public int CompareTo(Date other)
        {
            return DateId.CompareTo(other.DateId);
        }

        public override bool Equals(object obj)
        {
            {
                return obj is Date date &&
                DateId == DateId &&
                EventDate == date.EventDate &&
                EventName == date.EventName &&
                CountryId == date.CountryId &&
                EventTypeId == date.EventTypeId &&
                VictimsNumber == date.VictimsNumber;
            }
        }

        public override int GetHashCode()
        {
            int hashCode = -1508905540;
            hashCode = hashCode * -1521134295 + DateId.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<DateTime>.Default.GetHashCode(EventDate);
            //hashCode = hashCode * -1521134295 + EventDate.GetHashCode();
            hashCode = hashCode * -1521134295 + EventName.GetHashCode();
            hashCode = hashCode * -1521134295 + CountryId.GetHashCode();
            hashCode = hashCode * -1521134295 + EventTypeId.GetHashCode();
            hashCode = hashCode * -1521134295 + VictimsNumber.GetHashCode();
            return hashCode;
        }
    }
}
