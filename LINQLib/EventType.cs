﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Linq.Mapping;

namespace LINQLib
{
    [Table(Name = "EventTypes")]
    public class EventType : IComparable<EventType>
    {
        [Column(Name = "EventTypeId", IsPrimaryKey = true, IsDbGenerated = true)]
        public Int32 EventTypeId { get; set; }
        [Column(Name = "EventTypeName")]
        public String EventTypeName { get; set; }

        public int CompareTo(EventType other)
        {
            return EventTypeId.CompareTo(other.EventTypeId);
        }

        public override bool Equals(object obj)
        {
            return obj is EventType eventType &&
                EventTypeId == eventType.EventTypeId &&
                EventTypeName == eventType.EventTypeName;
        }

        public override int GetHashCode()
        {
            int hashCode = -329867721;
            hashCode = hashCode * -1521134295 + EventTypeId.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(EventTypeName);
            return hashCode;
        }
    }
}
