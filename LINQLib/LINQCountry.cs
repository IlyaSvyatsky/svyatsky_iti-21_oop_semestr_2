﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQLib
{
    public class LINQCountry
    {
        private DataContext db;
        public LINQCountry(string connectionString)
        {
            db = new DataContext(connectionString);
        }
        public Country GetIndexByName(string name)
        {
            var query = from country in db.GetTable<Country>()
                        where country.CountryName == name
                        select country;
            return query.First();
        }

        public Country GetCountryByIndex(int id)
        {
            var query = from country in db.GetTable<Country>()
                        where country.CountryId == id
                        select country;
            return query.First();
        }

        public Country[] GetAllCountries()
        {
            var query = from country in db.GetTable<Country>()
                        select country;
            List<Country> countries = new List<Country>();
            foreach (Country country in query)
            {
                countries.Add(country);
            }
            return countries.ToArray();
        }
    }
}
