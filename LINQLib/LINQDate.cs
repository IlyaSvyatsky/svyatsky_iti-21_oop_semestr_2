﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace LINQLib
{
    public class LINQDate
    {
        private DataContext db;
        public LINQDate(string connectionString)
        {
            db = new DataContext(connectionString);
        }

        public Date GetMusicByIndex(int id)
        {
            var query = from date in db.GetTable<Date>()
                        where date.DateId == id
                        select date;
            return query.First();
        }

        public int[] GetAllIds()
        {
            var query = from date in db.GetTable<Date>()
                        select date.DateId;
            List<int> ids = new List<int>();
            foreach (int dateId in query)
            {
                ids.Add(dateId);
            }
            return ids.ToArray();
        }

        public Date[] GetAllDates()
        {
            var query = from date in db.GetTable<Date>()
                        select date;
            List<Date> dates = new List<Date>();
            foreach (Date date in query)
            {
                dates.Add(date);
            }
            return dates.ToArray();
        }

        public bool Insert(Date date)
        {
            db.GetTable<Date>().InsertOnSubmit(date);
            try
            {
                db.SubmitChanges();
            }
            catch (Exception) { }
            return true;
        }

        public bool Update(Date date)
        {
            Date oldDate = GetMusicByIndex(date.DateId);
            oldDate.EventDate = date.EventDate;
            oldDate.EventName = date.EventName;
            oldDate.CountryId = date.CountryId;
            oldDate.EventTypeId = date.EventTypeId;
            oldDate.VictimsNumber = date.VictimsNumber;
            try
            {
                db.SubmitChanges();
            }
            catch (Exception) { }
            return true;
        }

        public bool Delete(Date date)
        {
            db.GetTable<Date>().DeleteOnSubmit(date);
            try
            {
                db.SubmitChanges();
            }
            catch (Exception) { }
            return true;
        }

        public string[] GetDateInfo()
        {
            Date[] dates = GetAllDates();
            string[] datesInfo = new string[dates.Length];
            for (int i = 0; i < dates.Length; i++)
            {
                var queryCountry = from country in db.GetTable<Country>()
                                 where country.CountryId == dates[i].CountryId
                                 select country;

                var queryEventType = from eventType in db.GetTable<EventType>()
                                     where eventType.EventTypeId == dates[i].EventTypeId
                                     select eventType;

                datesInfo[i] = string.Format("{0,-5} {1,-20} {2,-30} {3, -15} {4,-15} {5, -15}",
                    dates[i].DateId,
                    dates[i].EventDate.ToString(@"dd\-MM\-yyyy"),
                    dates[i].EventName,
                    queryCountry.First().CountryName,
                    queryEventType.First().EventTypeName,
                    dates[i].VictimsNumber
                    );
            }
            return datesInfo;
        }
    }
}
