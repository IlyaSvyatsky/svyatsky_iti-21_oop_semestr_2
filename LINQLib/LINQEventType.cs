﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQLib
{
    public class LINQEventType
    {
        private DataContext db;
        public LINQEventType(string connectionString)
        {
            db = new DataContext(connectionString);
        }
        public EventType GetIndexByName(string name)
        {
            var query = from eventType in db.GetTable<EventType>()
                        where eventType.EventTypeName == name
                        select eventType;
            return query.First();
        }

        public EventType GetGenreByIndex(int id)
        {
            var query = from eventType in db.GetTable<EventType>()
                        where eventType.EventTypeId == id
                        select eventType;
            return query.First();
        }

        public EventType[] GetAllEventTypes()
        {
            var query = from eventType in db.GetTable<EventType>()
                        select eventType;
            List<EventType> eventTypes = new List<EventType>();
            foreach (EventType eventType in query)
            {
                eventTypes.Add(eventType);
            }
            return eventTypes.ToArray();
        }
    }
}
