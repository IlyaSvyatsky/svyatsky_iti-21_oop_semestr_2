﻿using System;
using System.Windows.Forms;
using System.Drawing;
using Lib_Lab4;
using System.Collections.Generic;
using System.Windows.Forms.DataVisualization.Charting;

namespace Lab4_App
{
    public partial class Form1 : Form
    {
        //GroupBox для группировки CheckBox
        GroupBox GroupGraphics = new GroupBox();
        // Список отмеченных флажков
        private List<CheckBox> Selections = new List<CheckBox>();
        //Переменная для взаимодействия точек(считывания точек из файла).
        InteractionWithPoints Points = new InteractionWithPoints();
        //Список точек.
        List<Points> PointsForDrawing;
        //Лимит количества графиков.
        int limit = 4;
        //Количество выбранных графиков.
        int ActualNumberGrapchics = 0;
        //Chart для отображения графика.
        Chart ActualChart = new Chart();

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //Размер окна и название окна.
            this.Size = new System.Drawing.Size(940, 540);
            this.Text = "Lab4";

            //Создание кнопки для отрисовки.
            Button DrawingGraphics = new Button();
            //Местоположение.
            DrawingGraphics.Location = new System.Drawing.Point(0, 440);
            //Размер.
            DrawingGraphics.Size = new System.Drawing.Size(155, 54);
            //Отображаемый текст.
            DrawingGraphics.Text = "Вывести все\nвыбранные графики";
            //Привязка к событию через лямбду.
            DrawingGraphics.Click += (o, i) => { DrawGrapchics(); };
            //Добавление в элементы управления.
            this.Controls.Add(DrawingGraphics);

            //Местоположение.
            ActualChart.Location = new Point(155, 0);
            //Размер.
            ActualChart.Size = new Size(785, 530);
            //Создание площади для графика.
            ActualChart.ChartAreas.Add(new ChartArea());
            //Добавление в элементы управления.
            this.Controls.Add(ActualChart);

            GroupGraphics.Location = new Point(0, 10);
            GroupGraphics.Size = new Size(155, 200);
            GroupGraphics.Text = "Графики";
            this.Controls.Add(GroupGraphics);

            CheckBox SinCheckBox = new CheckBox();
            SinCheckBox.Location = new Point(0, 30);
            SinCheckBox.Text = "y = sin(x)";
            SinCheckBox.CheckedChanged += CheckBox_CheckedChanged;
            this.Controls.Add(SinCheckBox);
            GroupGraphics.Controls.Add(SinCheckBox);

            CheckBox CosCheckBox = new CheckBox();
            CosCheckBox.Location = new Point(0, 55);
            CosCheckBox.Text = "y = cos(x)";
            CosCheckBox.CheckedChanged += CheckBox_CheckedChanged;
            this.Controls.Add(CosCheckBox);
            GroupGraphics.Controls.Add(CosCheckBox);

            CheckBox LineCheckBox = new CheckBox();
            LineCheckBox.Location = new Point(0, 80);
            LineCheckBox.Text = "y = x";
            LineCheckBox.CheckedChanged += CheckBox_CheckedChanged;
            this.Controls.Add(LineCheckBox);
            GroupGraphics.Controls.Add(LineCheckBox);

            CheckBox QuadCheckBox = new CheckBox();
            QuadCheckBox.Location = new Point(0, 105);
            QuadCheckBox.Text = "y = x^2";
            QuadCheckBox.CheckedChanged += CheckBox_CheckedChanged;
            this.Controls.Add(QuadCheckBox);
            GroupGraphics.Controls.Add(QuadCheckBox);

            CheckBox FromFileCheckBox = new CheckBox();
            FromFileCheckBox.Location = new Point(0, 130);
            FromFileCheckBox.Text = "По точкам из файла";
            FromFileCheckBox.CheckedChanged += CheckBox_CheckedChanged;
            this.Controls.Add(FromFileCheckBox);
            GroupGraphics.Controls.Add(FromFileCheckBox);
        }

        private void CheckBox_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chk = sender as CheckBox;
            if (chk.Checked)
            {
                // Добавляем в список
                Selections.Add(chk);

                // Проверка на превышение
                if (Selections.Count > limit)
                {
                    // Удаляем самый первый флажок из списка
                    Selections[0].Checked = false;
                }
            }
            else
            {
                // Удаляем вхождение.
                Selections.Remove(chk);
            }
        }

        private void DrawGrapchics()
        {
            //Очистка всех графиков.
            ActualChart.Series.Clear();

            //Перебор всех выбранных графиков.
            for (int i = 0; i < Selections.Count; i++)
            {
                //Перебор через switch... case
                switch (Selections[i].Text)
                {
                    case "y = sin(x)":
                        Series SinSeriesOfPoint = new Series("Sin(x)");
                        SinSeriesOfPoint.ChartType = SeriesChartType.Spline;
                        const int N = 10;

                        for (double k = 0.02; k < N; k += 0.02)
                        {
                            double y = Math.Sin(k);
                            SinSeriesOfPoint.Points.AddXY(k, y);
                        }
                        ActualChart.Series.Add(SinSeriesOfPoint);
                        break;
                    case "y = cos(x)":
                        Series CosSeriesOfPoint = new Series("Cos(x)");
                        CosSeriesOfPoint.ChartType = SeriesChartType.Spline;

                        for (double k = 0.02; k < N; k += 0.02)
                        {
                            double y = Math.Cos(k);
                            CosSeriesOfPoint.Points.AddXY(k, y);
                        }
                        ActualChart.Series.Add(CosSeriesOfPoint);
                        break;
                    case "y = x":
                        Series LineSeriesOfPoint = new Series("Line");
                        LineSeriesOfPoint.ChartType = SeriesChartType.Spline;

                        for (double k = 0.02; k < 3; k += 0.02)
                        {
                            LineSeriesOfPoint.Points.AddXY(k, k);
                        }
                        ActualChart.Series.Add(LineSeriesOfPoint);
                        break;
                    case "y = x^2":
                        Series QuadSeriesOfPoint = new Series("Quad");
                        QuadSeriesOfPoint.ChartType = SeriesChartType.Spline;

                        for (double k = 0.02; k < 3; k += 0.02)
                        {
                            double y = k * k;
                            QuadSeriesOfPoint.Points.AddXY(k, y);
                        }
                        ActualChart.Series.Add(QuadSeriesOfPoint);
                        break;
                    case "По точкам из файла":
                        PointsForDrawing = new List<Points>();

                        Points.FillingValues(10, 5);
                        Points.SendCoord = AddNewPoint;
                        Points.GetPoints();

                        Series FromFileSeriesOfPoint = new Series("File");
                        FromFileSeriesOfPoint.ChartType = SeriesChartType.Spline;

                        for (int k = 0; k < PointsForDrawing.Count; k++)
                        {
                            FromFileSeriesOfPoint.Points.AddXY(PointsForDrawing[k].GetX(), PointsForDrawing[k].GetY());
                        }

                        ActualChart.Series.Add(FromFileSeriesOfPoint);
                        break;
                }
            }
        }

        //Реализация делегата по методу.
        private void AddNewPoint(double X, double Y)
        {
            PointsForDrawing.Add(new Points(X, Y));
        }
    }
}
