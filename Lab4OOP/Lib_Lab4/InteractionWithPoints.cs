﻿using System.IO;
using System;

namespace Lib_Lab4
{
    public class InteractionWithPoints
    {
        //Делегат для общения с формой.
        public Action<double, double> SendCoord { get; set; }
        //Путь к файлу.
        string path = @"D:\programming\2 cource\4 sem\Svyatsky_OOP_Sem1\Lab4OOP\Lib_Lab4\Points.txt";
        //Переменная рандома.
        private Random rnd = new Random();

        /// <summary>
        /// Метод для считывания точек.
        /// </summary>
        public void GetPoints()
        {
            using (StreamReader sr = new StreamReader(path, System.Text.Encoding.Default))
            {
                string line;

                while ((line = sr.ReadLine()) != null)
                {
                    String[] Points = line.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                    if (Double.TryParse(Points[0], out _) && Double.TryParse(Points[1], out _))
                    {
                        SendCoord(double.Parse(Points[0]), double.Parse(Points[1]));
                    }
                }
            }
        }

        /// <summary>
        /// Заполнения файла с точками.
        /// </summary>
        /// <param name="length"></param>
        /// <param name="maxValue"></param>
        public void FillingValues(double length, int maxValue)
        {
            using (StreamWriter writeRandomValue = new StreamWriter(path, false, System.Text.Encoding.Default))
            {
                for (double i = 0; i < length; i += 0.2)
                {
                    int ValueY = rnd.Next(0, maxValue);
                    writeRandomValue.WriteLine(i.ToString() + " " + ValueY);
                }
            }
        }
    }
}
