﻿using System;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;
using Lib_Lab5;

namespace Lab5_App
{
    public partial class Form1 : Form
    {
        //Для генерации случайных значений.
        Random rnd = new Random();
        //PictureBox на котором отображается круг.
        public PictureBox PictureForDrawing = new PictureBox();
        //Экземпляр круга.
        public Circle MainCircle;
        //Цвет круга.
        public Color ActualColor = Color.Violet;

        public Form1()
        {
            InitializeComponent();
        }

        //Реализация собственного события.
        private void MainCircle_GetCollisionResult(object sender, EventChancesDetected e)
        {
            //Если столкновение произошло выполняется условие.
            if(e.Result)
            {
                //Цвет круга меняется на случайный.
                ActualColor = Color.FromArgb(rnd.Next(256), rnd.Next(256), rnd.Next(256));
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //Размер окна и название окна.
            this.Size = new System.Drawing.Size(540, 540);
            this.Text = "Lab5";
            //Подписка на события нажатия клавиш.
            this.KeyDown += Form1_KeyDown1;

            //Инициализация PictureBox для рисования.
            //Местоположение.
            PictureForDrawing.Location = new System.Drawing.Point(0, 0);
            //Размер окна.
            PictureForDrawing.Size = new System.Drawing.Size(520, 490);
            //Цвет фона.
            PictureForDrawing.BackColor = Color.White;
            //Добавление в элементы управления.
            this.Controls.Add(PictureForDrawing);

            //Инициализация нового круга.
            MainCircle = new Circle(this.Width / 2, this.Height / 2, this.Height * 0.05,
                this.Width * 0.02f, this.Height * 0.02f);
            //Подписка на собственное событие.
            MainCircle.GetCollisionResult += MainCircle_GetCollisionResult;

            CircleMove();
        }

        private void Form1_KeyDown1(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Up) MainCircle.MoveAxisY(PictureForDrawing.Height, true);
            else if (e.KeyData == Keys.Down) MainCircle.MoveAxisY(PictureForDrawing.Height, false);

            if (e.KeyData == Keys.Right) MainCircle.MoveAxisX(PictureForDrawing.Width, true);
            else if (e.KeyData == Keys.Left) MainCircle.MoveAxisX(PictureForDrawing.Width, false);

            CircleMove();
        }

        public void CircleMove()
        {
            Bitmap bmp = new Bitmap(PictureForDrawing.Width,
                PictureForDrawing.Height);
            Graphics graph = Graphics.FromImage(bmp);

            //Очистка поля.
            graph.Clear(PictureForDrawing.BackColor);
            //Установка цвета кисти - цветом круга.
            SolidBrush ColorBrush = new SolidBrush(ActualColor);

            // Создание прямоугольника для эллипса.
            float x = (float)(MainCircle.CenterX - MainCircle.Radius);
            float y = (float)(MainCircle.CenterY - MainCircle.Radius);
            float width = (float)(MainCircle.Radius * 2);
            float height = (float)(MainCircle.Radius * 2);
            RectangleF rect = new RectangleF(x, y, width, height);

            //Вывод эллипса на экран.
            graph.FillEllipse(ColorBrush, rect);
            PictureForDrawing.Image = bmp;
        }
    }
}
