﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Lib_Lab5
{
    public class Circle
    {
        //Координата центра по X.
        public double CenterX { get; set; }
        //Координата центра по Y.
        public double CenterY { get; set; }
        //Радиус круга.
        public double Radius { get; set; }
        //Скорость по оси Х.
        public double AxisSpeedX { get; set; }
        //Скорость по оси Y.
        public double AxisSpeedY { get; set; }
        //Запоминает последнее направление по оси Х.
        public bool LastDirectionX { get; set; }
        //Запоминает последнее значение по оси Y.
        public bool LastDirectionY { get; set; }
        //Событие.
        public event EventHandler<EventChancesDetected> GetCollisionResult;

        //Конструктор круга.
        public Circle(double centerX, double centerY,
            double radius, double axisSpeedX, double axisSpeesY)
        {
            this.CenterX = centerX;
            this.CenterY = centerY;
            this.Radius = radius;
            this.AxisSpeedX = axisSpeedX;
            this.AxisSpeedY = axisSpeesY;
        }

        /// <summary>
        /// Перемещение по оси X.
        /// </summary>
        /// <param name="Width">Ширина окна</param>
        public void MoveAxisX(int Width, bool Direction)
        {
            //Если нет столкновения с границами происходит перемещение.
            if (Direction && CenterX + Radius <= Width)
            {
                CenterX += AxisSpeedX;
            }
            else if (!Direction && CenterX - Radius >= 0)
            {
                CenterX -= AxisSpeedX;
            }

            //Если была осуществлена подписка вызывается событие.
            if (LastDirectionX != Direction && GetCollisionResult != null)
            {
                GetCollisionResult(this, new EventChancesDetected(true));
            }
            //Запоминание последнего значения по оси Х.
            LastDirectionX = Direction;
        }

        /// <summary>
        /// Перемещение круга по оси Y.
        /// </summary>
        /// <param name="Hieght">Высота окна</param>
        public void MoveAxisY(int Hieght, bool Direction)
        {
            //Если нет столкновения со стенками передвигается
            if (Direction && CenterY - Radius >= 0)
            {
                CenterY -= AxisSpeedY;
            }
            else if (!Direction && CenterY + Radius <= Hieght)
            {
                CenterY += AxisSpeedY;
            }

            //Если была осуществлена подписка вызывается событие.
            if (LastDirectionY != Direction && GetCollisionResult != null)
            {
                GetCollisionResult(this, new EventChancesDetected(true));
            }
            //Запоминание последнего направления.
            LastDirectionY = Direction;
        }
    }
}
