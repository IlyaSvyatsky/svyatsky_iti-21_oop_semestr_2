﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib_Lab5
{
    /// <summary>
    /// Собственное событие.
    /// </summary>
    public class EventChancesDetected : EventArgs
    {
        //Результат изменения направления.
        public bool Result { get; private set; }
        public EventChancesDetected(bool result)
        {
            Result = result;
        }
    }
}
