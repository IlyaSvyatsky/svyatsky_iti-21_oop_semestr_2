using System;
using Xunit;
using Lib_Lab5;
using System.Drawing;

namespace Lib_Lab5Tests
{
    public class CircleTests
    {
        /// <summary>
        /// ��������� ������������ ����������� �� ��� X.
        /// </summary>
        [Fact]
        public void MoveTestX1()
        {
            //arrange
            Circle TestCircle = new Circle(0, 0, 5, 2, 2);
            Point expected = new Point(2, 0);
            //act
            TestCircle.MoveAxisX(7, true);
            Point actual = new Point((int)TestCircle.CenterX,
                (int)TestCircle.CenterY);
            //assert
            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// ��������� ������������ ����������� �� ��� X.
        /// </summary>
        [Fact]
        public void MoveTestX2()
        {
            //arrange
            Circle TestCircle = new Circle(15, 15, 5, 2, 2);
            Point expected = new Point(13, 15);
            //act
            TestCircle.MoveAxisX(30, false);
            Point actual = new Point((int)TestCircle.CenterX,
                (int)TestCircle.CenterY);
            //assert
            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// ��������� ������������ ����������� �� ��� X.
        /// </summary>
        [Fact]
        public void MoveTestX3()
        {
            //arrange
            Circle TestCircle = new Circle(15, 15, 5, 2, 2);
            Point expected = new Point(17, 15);
            //act
            TestCircle.MoveAxisX(30, true);
            Point actual = new Point((int)TestCircle.CenterX,
                (int)TestCircle.CenterY);
            //assert
            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// ��������� ������������ ����������� �� ��� X.
        /// </summary>
        [Fact]
        public void MoveTestX4()
        {
            //arrange
            Circle TestCircle = new Circle(15, 15, 5, 2, 2);
            Point expected = new Point(15, 15);
            //act
            TestCircle.MoveAxisX(16, true);
            Point actual = new Point((int)TestCircle.CenterX,
                (int)TestCircle.CenterY);
            //assert
            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// ��������� ������������ ����������� �� ��� Y.
        /// </summary>
        [Fact]
        public void MoveTestY1()
        {
            //arrange
            Circle TestCircle = new Circle(0, 0, 5, 2, 2);
            Point expected = new Point(0, 2);
            //act
            TestCircle.MoveAxisY(7, false);
            Point actual = new Point((int)TestCircle.CenterX,
                (int)TestCircle.CenterY);
            //assert
            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// ��������� ������������ ����������� �� ��� Y.
        /// </summary>
        [Fact]
        public void MoveTestY2()
        {
            //arrange
            Circle TestCircle = new Circle(15, 15, 5, 2, 2);
            Point expected = new Point(15, 13);
            //act
            TestCircle.MoveAxisY(30, true);
            Point actual = new Point((int)TestCircle.CenterX,
                (int)TestCircle.CenterY);
            //assert
            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// ��������� ������������ ����������� �� ��� Y.
        /// </summary>
        [Fact]
        public void MoveTestY3()
        {
            //arrange
            Circle TestCircle = new Circle(15, 15, 5, 2, 2);
            Point expected = new Point(15, 17);
            //act
            TestCircle.MoveAxisY(30, false);
            Point actual = new Point((int)TestCircle.CenterX,
                (int)TestCircle.CenterY);
            //assert
            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// ��������� ������������ ����������� �� ��� Y.
        /// </summary>
        [Fact]
        public void MoveTestY4()
        {
            //arrange
            Circle TestCircle = new Circle(15, 15, 5, 2, 2);
            Point expected = new Point(15, 15);
            //act
            TestCircle.MoveAxisY(16, false);
            Point actual = new Point((int)TestCircle.CenterX,
                (int)TestCircle.CenterY);
            //assert
            Assert.Equal(expected, actual);
        }
    }
}
