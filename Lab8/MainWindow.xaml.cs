﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using LINQLib;

namespace Lab8
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        LINQDate lINQDate;
        LINQCountry lINQCountry;
        LINQEventType lINQEventType;
        double widthListBox = 492;

        public MainWindow()
        {
            InitializeComponent();
            string connectionString = @"Data Source=.\SQLEXPRESS;Initial Catalog=GreatDatesbd;Integrated Security=True";
            lINQDate = new LINQDate(connectionString);
            lINQEventType = new LINQEventType(connectionString);
            lINQCountry = new LINQCountry(connectionString);
            UpdateListBox();
            Country[] countries = lINQCountry.GetAllCountries();
            for (int i = 0; i < countries.Length; i++)
            {
                comboBoxCountry.Items.Add(countries[i].CountryName);
            }
            EventType[] eventTypes = lINQEventType.GetAllEventTypes();
            for (int i = 0; i < eventTypes.Length; i++)
            {
                comboBoxEventType.Items.Add(eventTypes[i].EventTypeName);
            }
        }

        private void UpdateListBox()
        {
            listBox.Items.Clear();
            comboBoxId.Items.Clear();
            string[] dates = lINQDate.GetDateInfo();
            listBox.Items.Add(String.Format("{0,-5} {1,-20} {2,-30} {3, -15} {4,-15} {5, -15}",
                "Id",
                "EventDate",
                "EventName",
                "CountryName",
                "EventTypeName",
                "VictimsNumber"));
            for (int i = 0; i < dates.Length; i++)
            {
                listBox.Items.Add(dates[i]);
            }
            int[] ids = lINQDate.GetAllIds();
            for (int i = 0; i < dates.Length; i++)
            {
                comboBoxId.Items.Add(ids[i]);
            }
            comboBoxId.SelectedIndex = 0;
        }

        private void ChangeId(object sender, SelectionChangedEventArgs e)
        {
            if (comboBoxId.Items.Count > 0)
            {
                Date date = lINQDate.GetMusicByIndex((int)comboBoxId.SelectedItem);
                textBoxDate.Text = date.EventDate.ToString(@"dd\-MM\-yyyy");
                textBoxEventName.Text = date.EventName;
                comboBoxCountry.SelectedIndex = date.CountryId - 1;
                comboBoxEventType.SelectedIndex = date.EventTypeId - 1;
                textBoxVictimsNumber.Text = date.VictimsNumber;
            }
        }

        private void buttonInsert_Click(object sender, RoutedEventArgs e)
        {
            lINQDate.Insert(GetDate());
            UpdateListBox();
        }

        private void buttonUpdate_Click(object sender, RoutedEventArgs e)
        {
            lINQDate.Update(GetDate());
            UpdateListBox();
        }

        private void buttonDelete_Click(object sender, RoutedEventArgs e)
        {
            lINQDate.Delete(lINQDate.GetMusicByIndex((int)comboBoxId.SelectedItem));
            UpdateListBox();
        }

        private Date GetDate()
        {
            if (!DateTime.TryParse(textBoxDate.Text, out DateTime result1)) result1 = DateTime.Now;
            Date date = new Date();
            date.DateId = (int)comboBoxId.SelectedItem;
            date.EventDate = result1;
            date.EventName = textBoxEventName.Text;
            date.CountryId = comboBoxCountry.SelectedIndex + 1;
            date.EventTypeId = comboBoxEventType.SelectedIndex + 1;
            date.VictimsNumber = textBoxVictimsNumber.Text;

            return date;
        }

        private void ResizeList(object sender, SizeChangedEventArgs e)
        {
            if (listBox.ActualWidth > 0) listBox.FontSize = 10 / widthListBox * listBox.ActualWidth;
        }
    }
}
