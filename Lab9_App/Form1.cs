﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace Lab9_App
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Путь к файлу
            var Tuple = System.IO.File.ReadAllText(@"D:\programming\2 cource\4 sem\Svyatsky_OOP_Sem1\Lab9_App\Student.txt").Split('\n')
                .Select( x => { string[] Students = x.Split(' '); return new { Name = Students[0], Mark = double.Parse(Students[1]) }; })
                .GroupBy(Student => new { Student.Name }).Select(Student => { return new { Name = Student.Key.Name, Mark = (Student.Sum(Mark => Mark.Mark) / Student.Count()) }; })
                .OrderByDescending(Student => Student.Mark);

            listBox1.Items.AddRange(Tuple.Select(Student => { return (Student.Name.ToString() + " - " + Student.Mark.ToString()); }).ToArray());
        }
    }
}
