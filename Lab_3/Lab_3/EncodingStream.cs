﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_3
{
    public class EncodingStream : Stream
    {
        private Stream _stream;
        private Encoding _encoding;


        public EncodingStream(Stream stream, Encoding encoding)
        {
            _stream = stream;
            _encoding = encoding;
        }

        public override bool CanRead => true;

        public override bool CanSeek => false;

        public override bool CanWrite => true;

        public override long Length => throw new NotImplementedException();

        public override long Position { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public override void Flush()
        {
            throw new NotImplementedException();
        }

        public override int Read(byte[] array, int offset, int count)
        {
            int retValue = _stream.Read(array, offset, count);
            return retValue;
        }

        public string EncodingRead(byte[] array, int offset, int count)
        {
            Read(array, offset, count);
            return _encoding.GetString(array);
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            throw new NotImplementedException();
        }

        public override void SetLength(long value)
        {
            throw new NotImplementedException();
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            _stream.Write(buffer, offset, count);
        }
    }
}
