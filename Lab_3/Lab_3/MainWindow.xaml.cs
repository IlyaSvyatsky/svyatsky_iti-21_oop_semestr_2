﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Lab_3
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            encodingCBox.ItemsSource = Encoding.GetEncodings().Select(x => x.DisplayName);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if(encodingCBox.SelectedIndex != -1)
            {
                var dialog = new Microsoft.Win32.OpenFileDialog();
                dialog.DefaultExt = ".txt";
                dialog.Filter = "Text Files (*.txt)|*.txt";

                bool? result = dialog.ShowDialog();

                if (result == true)
                {
                    string path = dialog.FileName;
                    using (FileStream fstream = File.OpenRead(path))
                    {
                        using (EncodingStream encodingStream = new EncodingStream(fstream, Encoding.GetEncoding(Encoding.GetEncodings()[encodingCBox.SelectedIndex].CodePage)))
                        {
                            byte[] array = new byte[4096];

                            string textFromFile = encodingStream.EncodingRead(array, 0, array.Length);
                            mainDisplay.Text = textFromFile;
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("Необходимо выбрать кодировку");
            }
        }
    }
}
