﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DequeLibrary
{
    public class Deque<T> : ICollection<T>
    {
        object[] items;

        public Deque()
        {
            items = new object[0];
        }

        public int Count => items.Count();

        public bool IsReadOnly => false;

        public void PushFront(T item)
        {
            object[] tmpItems = new object[Count + 1];

            items.CopyTo(tmpItems, 1);
            tmpItems[0] = item;

            items = new object[tmpItems.Length];
            tmpItems.CopyTo(items, 0);
        }

        public T PopFront()
        {
            object item = items[0];
            object[] tmpItems = new object[Count - 1];

            Array.Copy(items, 1, tmpItems, 0, tmpItems.Length);
            items = new object[tmpItems.Length];
            tmpItems.CopyTo(items, 0);

            return (T)item;
        }

        public void PushBack(T item)
        {
            object[] tmpItems = new object[Count + 1];

            items.CopyTo(tmpItems, 0);
            tmpItems[tmpItems.Length - 1] = item;

            items = new object[tmpItems.Length];
            tmpItems.CopyTo(items, 0);
        }

        public T PopBack()
        {
            object item = items[Count - 1];
            object[] tmpItems = new object[Count - 1];

            Array.Copy(items, 0, tmpItems, 0, tmpItems.Length);
            items = new object[tmpItems.Length];
            tmpItems.CopyTo(items, 0);

            return (T)item;
        }

        public void Add(T item)
        {
            throw new NotImplementedException();
        }

        public void Clear()
        {
            items = new object[0];
        }

        public bool Contains(T item)
        {
            throw new NotImplementedException();
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            items.CopyTo(array, arrayIndex);
        }

        public IEnumerator<T> GetEnumerator()
        {
            for (int i = 0; i < items.Length; i++)
            {
                yield return (T)items[i];
            }
        }

        public bool Remove(T item)
        {
            throw new NotImplementedException();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
