﻿using DequeLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Lab_6
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Deque<string> deque = new Deque<string>();
        Deque<GreatDate> objectDeque = new Deque<GreatDate>();
        string[] items;
        GreatDate[] objectItems;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void UpdateStringDisplay()
        {
            items = new string[deque.Count];
            deque.CopyTo(items, 0);
            stringDiplay.ItemsSource = items;
        }

        private void UpdateObjectDisplay()
        {
            objectItems = new GreatDate[objectDeque.Count];
            objectDeque.CopyTo(objectItems, 0);
            objectDisplay.ItemsSource = objectItems;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            deque.PushFront(pFrontField.Text);
            pFrontField.Clear();
            UpdateStringDisplay();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            deque.PushBack(pBackField.Text);
            pBackField.Clear();
            UpdateStringDisplay();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            deque.PopFront();
            UpdateStringDisplay();
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            deque.PopBack();
            UpdateStringDisplay();
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            objectDeque.PushFront(new GreatDate(DateTime.Now.ToString(), oFrontField.Text));
            oFrontField.Clear();
            UpdateObjectDisplay();
        }

        private void Button_Click_5(object sender, RoutedEventArgs e)
        {
            objectDeque.PushBack(new GreatDate(DateTime.Now.ToString(), oBackField.Text));
            oBackField.Clear();
            UpdateObjectDisplay();
        }

        private void Button_Click_6(object sender, RoutedEventArgs e)
        {
            objectDeque.PopFront();
            UpdateObjectDisplay();
        }

        private void Button_Click_7(object sender, RoutedEventArgs e)
        {
            objectDeque.PopBack();
            UpdateObjectDisplay();
        }
    }
}
