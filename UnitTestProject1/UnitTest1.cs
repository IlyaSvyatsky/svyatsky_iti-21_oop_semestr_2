﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ComplexNumberLib;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            Random random = new Random();
            int iter = random.Next(1, 20000);
  
            for (int i = 0; i < iter; i++)
            {
                double a1 = Convert.ToDouble(random.Next(-100, 20)/10.0) + i;
                double b1 = -20 + i;
                double a2 = -9 + i;
                double b2 = 0 + i;

                ComplexNumber number1 = new ComplexNumber(a1, b1);
                ComplexNumber number2 = new ComplexNumber(a2, b2);
                ComplexNumber number3 = number1 * number2;

                string result = number3.GetNumber;

                double aa;
                double bb;
                aa = a1 * a2 - b1 * b2;
                bb = a1 * b2 + b1 * a2;
                if (bb > 0)
                {
                    Assert.AreEqual(result, Convert.ToString(aa) + "+" + Convert.ToString(bb) + "*i");
                }
                else
                {
                    Assert.AreEqual(result, Convert.ToString(aa) + Convert.ToString(bb) + "*i");
                }
            }
        }

        [TestMethod]
        public void TestMethod2()
        {
            int iter = 20;
            for(int i = 0; i < iter; i++)
            {
                double a1 = 1 + i;
                double b1 = 4 + i;
                double a2 = 3 + i;
                double b2 = 2 + i;

                ComplexNumber number1 = new ComplexNumber(a1, b1);
                ComplexNumber number2 = new ComplexNumber(a2, b2);
                ComplexNumber number3 = number1 + number2;

                string result = number3.GetNumber;

                double aa;
                double bb;
                aa = a1 + a2;
                bb = b1 + b2;
                Assert.AreEqual(result, Convert.ToString(aa) + "+" + Convert.ToString(bb) + "*i");
                
            }
        }

        [TestMethod]
        public void TestMethod3()
        {
            int iter = 100;
            for (int i = 0; i < iter; i++)
            {
                double a1 = 1 + i;
                double b1 = 4 + i;
                double a2 = 3 + i;
                double b2 = 2 + i;

                ComplexNumber number1 = new ComplexNumber(a1, b1);
                ComplexNumber number2 = new ComplexNumber(a2, b2);
                ComplexNumber number3 = number1 * number2;

                string result = number3.GetNumber;

                double aa;
                double bb;
                aa = a1*a2 - b1*b2;
                bb = a1 * b2 + b1 * a2;
                Assert.AreEqual(result, Convert.ToString(aa) + "+" + Convert.ToString(bb) + "*i");

            }
        }

        [TestMethod]
        public void TestMethod4()
        {
            int iter = 20;
            for (int i = 0; i < iter; i++)
            {
                double a1 = 1 + i;
                double b1 = 4 + i;
                double a2 = 3 + i;
                double b2 = 2 + i;

                ComplexNumber number1 = new ComplexNumber(a1, b1);
                ComplexNumber number2 = new ComplexNumber(a2, b2);
                ComplexNumber number3 = number1 * number2;

                string result = number3.GetNumber;

                double aa;
                double bb;
                aa = a1 * a2 - b1 * b2;
                bb = a1 * b2 + b1 * a2;
                Assert.AreEqual(result, Convert.ToString(aa) + "+" + Convert.ToString(bb) + "*i");
            }
        }

        //на создание класса
        [TestMethod]
        public void TestMethod5()
        {
            ComplexNumber number1 = new ComplexNumber();

            Assert.AreNotEqual(number1, null);
        }

        [TestMethod]
        public void TestMethod6()
        {
            ComplexNumber number2 = new ComplexNumber();

            Assert.AreNotEqual(number2, null);
        }

        //prisvoenie znacheniya
        [TestMethod]
        public void TestMethod7()
        {
            int iter = 20;
            for (int i = 0; i < iter; i++)
            {            
                ComplexNumber number1 = new ComplexNumber(i);

                Assert.AreEqual(number1.a, i);
            }
        }

        //присвоение значения
        [TestMethod]
        public void TestMethod8()
        {
            int iter = -1;
            for (int i = -20; i < iter; i++)
            {
                ComplexNumber number2 = new ComplexNumber(1, i);

                Assert.AreEqual(number2.b, i);
            }
        }

 
    }
}
