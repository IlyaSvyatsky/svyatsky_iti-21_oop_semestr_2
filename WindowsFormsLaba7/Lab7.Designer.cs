﻿namespace WindowsFormsLaba7
{
    partial class Lab7
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBox = new System.Windows.Forms.ListBox();
            this.textBoxVictimsNumber = new System.Windows.Forms.TextBox();
            this.labelVictimsNumber = new System.Windows.Forms.Label();
            this.labelEventName = new System.Windows.Forms.Label();
            this.textBoxEventName = new System.Windows.Forms.TextBox();
            this.comboBoxEventType = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.labelCountry = new System.Windows.Forms.Label();
            this.buttonInsert = new System.Windows.Forms.Button();
            this.buttonUpdate = new System.Windows.Forms.Button();
            this.buttonDelete = new System.Windows.Forms.Button();
            this.comboBoxInd = new System.Windows.Forms.ComboBox();
            this.labelD = new System.Windows.Forms.Label();
            this.textBoxDate = new System.Windows.Forms.TextBox();
            this.textBoxCountry = new System.Windows.Forms.TextBox();
            this.comboBoxEventName = new System.Windows.Forms.ComboBox();
            this.comboBoxCountry = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // listBox
            // 
            this.listBox.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.listBox.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.listBox.FormattingEnabled = true;
            this.listBox.Location = new System.Drawing.Point(187, 13);
            this.listBox.Name = "listBox";
            this.listBox.Size = new System.Drawing.Size(646, 485);
            this.listBox.TabIndex = 0;
            // 
            // textBoxVictimsNumber
            // 
            this.textBoxVictimsNumber.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.textBoxVictimsNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxVictimsNumber.Location = new System.Drawing.Point(3, 296);
            this.textBoxVictimsNumber.Name = "textBoxVictimsNumber";
            this.textBoxVictimsNumber.Size = new System.Drawing.Size(178, 29);
            this.textBoxVictimsNumber.TabIndex = 1;
            // 
            // labelVictimsNumber
            // 
            this.labelVictimsNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelVictimsNumber.Location = new System.Drawing.Point(1, 270);
            this.labelVictimsNumber.Name = "labelVictimsNumber";
            this.labelVictimsNumber.Size = new System.Drawing.Size(180, 23);
            this.labelVictimsNumber.TabIndex = 2;
            this.labelVictimsNumber.Text = "VictimsNumber";
            // 
            // labelEventName
            // 
            this.labelEventName.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelEventName.Location = new System.Drawing.Point(1, 139);
            this.labelEventName.Name = "labelEventName";
            this.labelEventName.Size = new System.Drawing.Size(180, 23);
            this.labelEventName.TabIndex = 6;
            this.labelEventName.Text = "EventName";
            // 
            // textBoxEventName
            // 
            this.textBoxEventName.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.textBoxEventName.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxEventName.Location = new System.Drawing.Point(3, 165);
            this.textBoxEventName.Name = "textBoxEventName";
            this.textBoxEventName.Size = new System.Drawing.Size(177, 29);
            this.textBoxEventName.TabIndex = 5;
            // 
            // comboBoxEventType
            // 
            this.comboBoxEventType.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.comboBoxEventType.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.comboBoxEventType.FormattingEnabled = true;
            this.comboBoxEventType.Items.AddRange(new object[] {
            "Война",
            "Конфликт",
            "Бой",
            "Революция",
            "Договор"});
            this.comboBoxEventType.Location = new System.Drawing.Point(5, 97);
            this.comboBoxEventType.Name = "comboBoxEventType";
            this.comboBoxEventType.Size = new System.Drawing.Size(176, 32);
            this.comboBoxEventType.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(1, 70);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(175, 24);
            this.label1.TabIndex = 7;
            this.label1.Text = "EventType";
            // 
            // labelCountry
            // 
            this.labelCountry.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelCountry.Location = new System.Drawing.Point(0, 207);
            this.labelCountry.Name = "labelCountry";
            this.labelCountry.Size = new System.Drawing.Size(180, 28);
            this.labelCountry.TabIndex = 9;
            this.labelCountry.Text = "Country";
            // 
            // buttonInsert
            // 
            this.buttonInsert.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.buttonInsert.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonInsert.Location = new System.Drawing.Point(3, 331);
            this.buttonInsert.Name = "buttonInsert";
            this.buttonInsert.Size = new System.Drawing.Size(178, 69);
            this.buttonInsert.TabIndex = 11;
            this.buttonInsert.Text = "Insert";
            this.buttonInsert.UseVisualStyleBackColor = false;
            this.buttonInsert.Click += new System.EventHandler(this.buttonInsert_Click);
            // 
            // buttonUpdate
            // 
            this.buttonUpdate.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.buttonUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonUpdate.Location = new System.Drawing.Point(4, 406);
            this.buttonUpdate.Name = "buttonUpdate";
            this.buttonUpdate.Size = new System.Drawing.Size(177, 62);
            this.buttonUpdate.TabIndex = 12;
            this.buttonUpdate.Text = "Update";
            this.buttonUpdate.UseVisualStyleBackColor = false;
            this.buttonUpdate.Click += new System.EventHandler(this.buttonUpdate_Click);
            // 
            // buttonDelete
            // 
            this.buttonDelete.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.buttonDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonDelete.Location = new System.Drawing.Point(5, 474);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(176, 67);
            this.buttonDelete.TabIndex = 13;
            this.buttonDelete.Text = "Delete";
            this.buttonDelete.UseVisualStyleBackColor = false;
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // comboBoxInd
            // 
            this.comboBoxInd.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.comboBoxInd.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.comboBoxInd.FormattingEnabled = true;
            this.comboBoxInd.Location = new System.Drawing.Point(187, 504);
            this.comboBoxInd.Name = "comboBoxInd";
            this.comboBoxInd.Size = new System.Drawing.Size(61, 37);
            this.comboBoxInd.TabIndex = 14;
            this.comboBoxInd.SelectedValueChanged += new System.EventHandler(this.DockIndexChanget);
            // 
            // labelD
            // 
            this.labelD.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelD.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.labelD.Location = new System.Drawing.Point(6, 6);
            this.labelD.Name = "labelD";
            this.labelD.Size = new System.Drawing.Size(174, 20);
            this.labelD.TabIndex = 16;
            this.labelD.Text = "Date";
            this.labelD.Click += new System.EventHandler(this.labelD_Click);
            // 
            // textBoxDate
            // 
            this.textBoxDate.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.textBoxDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxDate.Location = new System.Drawing.Point(5, 29);
            this.textBoxDate.Name = "textBoxDate";
            this.textBoxDate.Size = new System.Drawing.Size(175, 29);
            this.textBoxDate.TabIndex = 15;
            // 
            // textBoxCountry
            // 
            this.textBoxCountry.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.textBoxCountry.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxCountry.Location = new System.Drawing.Point(312, 512);
            this.textBoxCountry.Name = "textBoxCountry";
            this.textBoxCountry.Size = new System.Drawing.Size(176, 29);
            this.textBoxCountry.TabIndex = 17;
            // 
            // comboBoxEventName
            // 
            this.comboBoxEventName.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.comboBoxEventName.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.comboBoxEventName.FormattingEnabled = true;
            this.comboBoxEventName.Location = new System.Drawing.Point(613, 512);
            this.comboBoxEventName.Name = "comboBoxEventName";
            this.comboBoxEventName.Size = new System.Drawing.Size(175, 32);
            this.comboBoxEventName.TabIndex = 18;
            this.comboBoxEventName.SelectedIndexChanged += new System.EventHandler(this.EventNameChanged);
            // 
            // comboBoxCountry
            // 
            this.comboBoxCountry.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.comboBoxCountry.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.comboBoxCountry.FormattingEnabled = true;
            this.comboBoxCountry.Location = new System.Drawing.Point(3, 235);
            this.comboBoxCountry.Name = "comboBoxCountry";
            this.comboBoxCountry.Size = new System.Drawing.Size(175, 32);
            this.comboBoxCountry.TabIndex = 19;
            // 
            // Lab7
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(840, 546);
            this.Controls.Add(this.comboBoxCountry);
            this.Controls.Add(this.comboBoxEventName);
            this.Controls.Add(this.textBoxCountry);
            this.Controls.Add(this.labelD);
            this.Controls.Add(this.textBoxDate);
            this.Controls.Add(this.comboBoxInd);
            this.Controls.Add(this.buttonDelete);
            this.Controls.Add(this.buttonUpdate);
            this.Controls.Add(this.buttonInsert);
            this.Controls.Add(this.labelCountry);
            this.Controls.Add(this.comboBoxEventType);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labelEventName);
            this.Controls.Add(this.textBoxEventName);
            this.Controls.Add(this.labelVictimsNumber);
            this.Controls.Add(this.textBoxVictimsNumber);
            this.Controls.Add(this.listBox);
            this.Name = "Lab7";
            this.Text = "Lab7";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listBox;
        private System.Windows.Forms.TextBox textBoxVictimsNumber;
        private System.Windows.Forms.Label labelVictimsNumber;
        private System.Windows.Forms.Label labelEventName;
        private System.Windows.Forms.TextBox textBoxEventName;
        private System.Windows.Forms.ComboBox comboBoxEventType;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelCountry;
        private System.Windows.Forms.Button buttonInsert;
        private System.Windows.Forms.Button buttonUpdate;
        private System.Windows.Forms.Button buttonDelete;
        private System.Windows.Forms.ComboBox comboBoxInd;
        private System.Windows.Forms.Label labelD;
        private System.Windows.Forms.TextBox textBoxDate;
        private System.Windows.Forms.TextBox textBoxCountry;
        private System.Windows.Forms.ComboBox comboBoxEventName;
        private System.Windows.Forms.ComboBox comboBoxCountry;
    }
}


