﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GreatDatesLib;

namespace WindowsFormsLaba7
{
    public partial class Lab7 : Form
    {
        private int[] ids;
        public Lab7()
        {
            InitializeComponent();
            string connectionString = @"Data Source=.\SQLEXPRESS;Initial Catalog=GreatDatesbd;Integrated Security=True";
            //MSSQEventName.Initialization(connectionString);
            MSSQCountry.Initialization(connectionString);
            MSSQEventType.Initialization(connectionString);
            MSSQDate.Initialization(connectionString);
            //comboBoxEventName.Items.AddRange(MSSQEventName.GetEventNames());
            comboBoxCountry.Items.AddRange(MSSQCountry.GetCountries());
            comboBoxEventType.Items.AddRange(MSSQEventType.GetEventTypes());
            ids = MSSQDate.GetFirsIds();
            UpdateWindow();

        }

        private void UpdateWindow()
        {
            ids = MSSQDate.GetFirsIds();
            comboBoxInd.Items.Clear();
            listBox.Items.Clear();
            listBox.Items.Add(String.Format("{0,-5} {1,-20} {2,-30} {3, -15} {4,-15} {5, -15}",
                "Id",
                "EventDate",
                "EventName",
                "CountryName",
                "EventTypeName",
                "VictimsNumber"));
            string[] dates = MSSQDate.GetMusicInfo();
            for (int i = 0; i < dates.Length; i++)
            {
                comboBoxInd.Items.Add(ids[i]);
                listBox.Items.Add(dates[i]);
            }
            comboBoxInd.SelectedIndex = 0;
        }

        private void buttonInsert_Click(object sender, EventArgs e)
        {
            MSSQDate.Insert(GetDate());
            UpdateWindow();
        }

        private void DockIndexChanget(object sender, EventArgs e)
        {
            Date date = MSSQDate.GetMusicByIndex(ids[comboBoxInd.SelectedIndex]);

            textBoxDate.Text = date.EventDate.ToString(@"dd\-MM\-yyyy");
            //comboBoxEventName.SelectedIndex = date.EventNameId - 1;
            //textBoxCountry.Text = date.Country;
            textBoxEventName.Text = date.EventName;
            comboBoxCountry.SelectedIndex = date.CountryId - 1;
            comboBoxEventType.SelectedIndex = date.EventTypeId - 1;
            //numb = date.VictimsNumber;
            textBoxVictimsNumber.Text = date.VictimsNumber;
        }

        private Date GetDate()
        {
            if (!DateTime.TryParse(textBoxDate.Text, out DateTime dateTime)) dateTime = DateTime.Now;
           // Int32.TryParse(textBoxVictimsNumber.Text, out Int32 numb);
            return new Date(
                ids[comboBoxInd.SelectedIndex],
                dateTime,
                //MSSQEventName.GetIndexByName(comboBoxEventName.Text),
                textBoxEventName.Text,
                //textBoxCountry.Text,
                MSSQCountry.GetIndexByName(comboBoxCountry.Text),
                MSSQEventType.GetIndexByName(comboBoxEventType.Text),
                textBoxVictimsNumber.Text
                );
        }

        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            MSSQDate.Update(GetDate());
            UpdateWindow();
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            MSSQDate.Delete(GetDate());
            UpdateWindow();
        }

        private void labelD_Click(object sender, EventArgs e)
        {

        }

        private void EventNameChanged(object sender, EventArgs e)
        {
           //MSSQEventName.GetEventNameByIndex(comboBoxEventName.SelectedIndex + 1);
        }   

    }
}

