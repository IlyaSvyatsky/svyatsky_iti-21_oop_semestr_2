﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using XMLparseLib;
using System.Xml.Schema;


namespace Lab2
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<GreatDate> greatDates;
        private string selectedPath;

        public MainWindow()
        {
            InitializeComponent();
            greatDates = new List<GreatDate>();
        }

        private void RemoveB_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Int32.TryParse(indexT.Text, out Int32 index);
                greatDates.RemoveAt(index);
                UpdateMusics();
            }
            catch (Exception)
            {
                MessageBox.Show("Error");
            }

        }

        private void AddB_Click(object sender, RoutedEventArgs e)
        {
            GreatDate dates = new GreatDate();
            try
            {
                SetMusicAttrs(dates);
                greatDates.Add(dates);
                UpdateMusics();
            }
            catch (Exception)
            {
                MessageBox.Show("Error");
            }

        }

        private void OFB_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            if (dialog.ShowDialog() == true)
            {
                selectedPath = dialog.FileName;
                try
                {
                    GreatDateHandler handler = new GreatDateHandler();
                    GreatDateSAXParser parser = new GreatDateSAXParser(handler);
                    parser.Parse(selectedPath);
                    greatDates.Clear();
                    greatDates.AddRange(handler.GetGreatDates());
                    UpdateMusics();
                }
                catch (XmlSchemaValidationException)
                {
                    MessageBox.Show("Error");
                }
            }
        }

        private void SFB_Click(object sender, RoutedEventArgs e)
        {
            GreatDateHandler handler = new GreatDateHandler();
            GreatDateSAXParser parser = new GreatDateSAXParser(handler);
            parser.Save(selectedPath, greatDates.ToArray());
        }

        private void EditB_Click(object sender, RoutedEventArgs e)
        {
            GreatDate dates = new GreatDate();
            try
            {
                Int32.TryParse(indexT.Text, out Int32 index);
                SetMusicAttrs(dates);
                greatDates.RemoveAt(index);
                greatDates.Insert(index, dates);
                UpdateMusics();
            }
            catch (Exception)
            {
                MessageBox.Show("Error");
            }

        }

        private void UpdateMusics()
        {
            Int32 index = 0;
            String update = "GreatDates:\n";
            foreach (GreatDate greatDate in greatDates)
            {
                update += index++ + " " + greatDate.ToString() + "\n";
            }
            Info.Text = update;
        }

        private void SetMusicAttrs(GreatDate greatDate)
        {
            if (!Int32.TryParse(numberOfVictimsT.Text, out Int32 numberOfVictims))
            {
                throw new Exception("Invalide numberOfVictims.");
            }
            if (!Enum.TryParse(eventT.Text, out GreatDate.EventType eventType))
            {
                throw new Exception("Invalide Event.");
            }
            if (dateT.Text.Length != 10)
            {
                throw new Exception("Invalide data.");
            }

            greatDate.Name = nameT.Text;
            greatDate.NumberOfVictims = numberOfVictims;
            greatDate.Event = eventType;
            greatDate.Date = dateT.Text;
            greatDate.Country = countryT.Text;
        }

    }
}
