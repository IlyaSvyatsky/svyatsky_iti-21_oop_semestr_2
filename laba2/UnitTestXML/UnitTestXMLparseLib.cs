﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using XMLparseLib;
using System.Xml;
using System.Xml.Schema;

namespace UnitTestXML
{
    [TestClass]
    public class UnitTestXMLparseLib
    {
        [TestClass]
        public class ParserTests
        {
            [TestMethod]
            public void MedicineHandlerTest()
            {
                GreatDateHandler handler = new GreatDateHandler();
                SAXParser parser = new SAXParser(handler);
                string path = @"D:\programming\2 cource\4 sem\OOP\laba2\TestDate1.xml";

                parser.Parse(path);

                GreatDate[] expected = new GreatDate[2];
                expected[0] = new GreatDate
                {
                    Name = "WW1",
                    NumberOfVictims = 25,
                    Event = GreatDate.EventType.War,
                    Date = "2000-01-01",
                    Country = "Украина"
                };
                expected[1] = new GreatDate
                {
                    Name = "RBWorldWar1",
                    NumberOfVictims = 2500,
                    Event = GreatDate.EventType.Battle,
                    Date = "2001-11-01",
                    Country = "РБ"
                };

                CollectionAssert.AreEqual(expected, handler.GetGreatDates());
            }

            [TestMethod]
            [ExpectedException(typeof(XmlSchemaValidationException))]
            public void MedicineValidationTest()
            {
                GreatDateHandler handler = new GreatDateHandler();
                GreatDateSAXParser parser = new GreatDateSAXParser(handler);
                string path = @"D:\programming\2 cource\4 sem\OOP\laba2\TestDate2.xml";

                parser.Parse(path);
            }

            [TestMethod]
            public void WriterTest()
            {
                GreatDateHandler handler = new GreatDateHandler();
                GreatDateSAXParser parser = new GreatDateSAXParser(handler);
                string path = @"D:\programming\2 cource\4 sem\OOP\laba2\TestDate3.xml";

                GreatDate[] expected = new GreatDate[2];
                expected[0] = new GreatDate
                {
                    Name = "WorldWarTwo",
                    NumberOfVictims = 10000,
                    Event = GreatDate.EventType.Treaty,
                    Date = "1939-11-01",
                    Country = "World"
                };
                expected[1] = new GreatDate
                {
                    Name = "WW1",
                    NumberOfVictims = 25,
                    Event = GreatDate.EventType.War,
                    Date = "2000-01-01",
                    Country = "Украина"
                };

                parser.Save(path, expected);
                parser.Parse(path);

                CollectionAssert.AreEqual(expected, handler.GetGreatDates());
            }
        }

    }
}
