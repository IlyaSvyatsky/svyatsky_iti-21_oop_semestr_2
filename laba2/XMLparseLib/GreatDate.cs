﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XMLparseLib
{
    /// <summary>
    /// Class Dates
    /// </summary>
    public class GreatDate
    {
        /// <summary>
        /// Enumeration of possible genres
        /// </summary>
        public enum EventType
        {
            /// <summary>
            /// War
            /// </summary>
            War,
            /// <summary>
            /// Battle
            /// </summary>
            Battle,
            /// <summary>
            /// Revolution
            /// </summary>
            Revolution,
            /// <summary>
            /// Treaty
            /// </summary>
            Treaty
        }

        /// <summary>
        /// Property contains the date
        /// </summary>
        public string Date { get; set; }
        /// <summary>
        /// Property contains the name
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Property contains the country
        /// </summary>
        public string Country { get; set; }
        /// <summary>
        /// Property contains the event
        /// </summary>
        public EventType Event { get; set; }
        /// <summary>
        /// Property contains the victims
        /// </summary>
        public int NumberOfVictims { get; set; }

        /// <summary>
        /// GreatDate constructor 
        /// </summary>
        public GreatDate(string date = "1300-01-01", string name = "Unknown", string country = "Unknown",
        EventType eventType = EventType.War, int numberOfVictims = 10000000)
        {
            Date = date;
            Name = name;
            Country = country;
            Event = eventType;
            NumberOfVictims = numberOfVictims;
        }

        /// <summary>
        /// Method to strings
        /// </summary>
        public override string ToString()
        {
            return Name + " " + NumberOfVictims + " " + " " + Event.ToString() + " "
                + Date + " " + Country;
        }

        /// <summary>
        /// Method to string
        /// </summary>
        public override bool Equals(object obj)
        {
            {
                return obj is GreatDate greatDate &&
                       Date == greatDate.Date &&
                       Name == greatDate.Name &&
                       Country == greatDate.Country &&
                       Event == greatDate.Event &&
                       NumberOfVictims == greatDate.NumberOfVictims;
            }

        }

        /// <summary>
        /// ХешКод
        /// </summary>
        /// <returns>ХешКод</returns>
        public override int GetHashCode()
        {
            int hashCode = -210507647;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Date);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Name);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Country);
            hashCode = hashCode * -1521134295 + Event.GetHashCode();
            hashCode = hashCode * -1521134295 + NumberOfVictims.GetHashCode();
            return hashCode;
        }
    }
}

