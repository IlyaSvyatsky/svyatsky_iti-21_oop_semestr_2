﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XMLparseLib
{
    /// <summary>
    /// Class GreatDateHandler
    /// </summary>
    public class GreatDateHandler
    {
        private enum ElementName
        {
            GreatDates,
            GreatDate,
            Name,
            NumberOfVictims,
            Event,
            Date, 
            Country,
            None
        }

        private ElementName currentElement;
        private GreatDate current;
        private LinkedList<GreatDate> greatDates;

        /// <summary>
        /// GreatDate Handler constructor
        /// </summary>
        public GreatDateHandler()
        {
            currentElement = ElementName.None;
            greatDates = new LinkedList<GreatDate>();
        }
        /// <summary>
        /// Processing the end of an element 
        /// </summary>
        /// <param name="name">Element name</param>
        public void EndElement(string name)
        {
            currentElement = ElementName.None;
            if (name == "GreatDate")
            {
                greatDates.AddLast(current);
            }
        }
        /// <summary>
        /// Processing the start of an element 
        /// </summary>
        /// <param name="name">Element name</param>
        public void StartElement(string name)
        {
            ElementName elementName;
            if (Enum.TryParse(name, out elementName))
            {
                currentElement = elementName;
                if (elementName == ElementName.GreatDate)
                {
                    current = new GreatDate();
                }
            }
        }
        /// <summary>
        /// Processing the text in an element 
        /// </summary>
        /// <param name="text">Element text</param>
        public void Text(string text)
        {
            if (current == null)
            {
                return;
            }
            switch (currentElement)
            {
                case ElementName.Name:
                    current.Name = text;
                    break;
                case ElementName.NumberOfVictims:
                    Int32 numberOfVictims;
                    Int32.TryParse(text, out numberOfVictims);
                    current.NumberOfVictims = numberOfVictims;
                    break;
                case ElementName.Event:
                    GreatDate.EventType eventType;
                    Enum.TryParse(text, out eventType);
                    current.Event = eventType;
                    break;
                case ElementName.Date:
                    current.Date = text;
                    break;
                case ElementName.Country:
                    current.Country = text;
                    break;
            }
        }

        /// <summary>
        /// Convert date list in array
        /// </summary>
        /// <returns>Return music array</returns>
        public GreatDate[] GetGreatDates()
        {
            GreatDate[] greatDates = new GreatDate[this.greatDates.Count];
            this.greatDates.CopyTo(greatDates, 0);
            return greatDates;
        }

    }
}
