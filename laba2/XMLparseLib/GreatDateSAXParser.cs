﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Schema;

namespace XMLparseLib
{
    /// <summary>
    /// Class DateSAXParser
    /// </summary>
    public class GreatDateSAXParser : SAXParser
    {
        private const string XSD_PATH = "http://tempuri.org/DateXMLSchema.xsd";
        private const string XSD_NAME = "D:\\programming\\2 cource\\4 sem\\OOP\\laba2\\XMLparseLib\\DateXMLSchema.xsd";

        /// <summary>
        /// DateSaxParser constructor 
        /// </summary>
        /// <param name="handler">Music handler</param>
        public GreatDateSAXParser(GreatDateHandler handler) : base(handler)
        {
        }

        /// <summary>
        /// Method parse
        /// </summary>
        /// <param name="path">Path of file</param>
        public void Parse(string path)
        {
            XmlReaderSettings settings = new XmlReaderSettings();
            settings.Schemas.Add(XSD_PATH, XSD_NAME);
            settings.ValidationType = ValidationType.Schema;
            settings.ValidationEventHandler += new ValidationEventHandler(MedicineValidationEventHandler);

            Parse(path, settings);
        }

        /// <summary>
        /// Method save file
        /// </summary>
        /// <param name="path">Path of file</param>
        /// <param name="greatDates">Array of musics</param>
        public void Save(string path, GreatDate[] greatDates)
        {
            using (XmlWriter writer = XmlWriter.Create(path))
            {
                writer.WriteStartDocument();
                writer.WriteWhitespace("\n");
                writer.WriteStartElement("GreatDates", XSD_PATH);
                writer.WriteWhitespace("\n");

                foreach (GreatDate greatDate in greatDates)
                {
                    writer.WriteStartElement("GreatDate");
                    writer.WriteWhitespace("\n\t");

                    WriteElementWithText(writer, "Name", greatDate.Name);
                    writer.WriteWhitespace("\n\t");
                    WriteElementWithText(writer, "NumberOfVictims", greatDate.NumberOfVictims.ToString());
                    writer.WriteWhitespace("\n\t");
                    WriteElementWithText(writer, "Event", greatDate.Event.ToString());
                    writer.WriteWhitespace("\n\t");
                    WriteElementWithText(writer, "Date", greatDate.Date.ToString());
                    writer.WriteWhitespace("\n\t");
                    WriteElementWithText(writer, "Country", greatDate.Country);
                    writer.WriteWhitespace("\n");

                    writer.WriteEndElement();
                    writer.WriteWhitespace("\n");
                }

                writer.WriteEndElement();
                writer.WriteEndDocument();

            }
        }

        private void WriteElementWithText(XmlWriter writer, string name, string text)
        {
            writer.WriteStartElement(name);
            writer.WriteString(text);
            writer.WriteEndElement();
        }

        private static void MedicineValidationEventHandler(object sender, ValidationEventArgs e)
        {
            if (e.Severity == XmlSeverityType.Warning ||
                e.Severity == XmlSeverityType.Error)
            {
                throw new XmlSchemaValidationException();
            }
        }
    }
}

