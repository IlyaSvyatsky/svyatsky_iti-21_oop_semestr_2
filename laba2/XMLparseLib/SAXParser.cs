﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace XMLparseLib
{
    /// <summary>
    /// Class SaxParser
    /// </summary>
    public class SAXParser
    {
        private GreatDateHandler handler;


        /// <summary>
        /// SaxParser constructor 
        /// </summary>
        public SAXParser(GreatDateHandler handler)
        {
            this.handler = handler;
        }

        /// <summary>
        /// Method parse
        /// </summary>
        /// <param name="path">Path of file</param>
        /// <param name="settings">Xsd settings</param>
        public void Parse(string path, XmlReaderSettings settings = null)
        {
            if (settings == null)
            {
                settings = new XmlReaderSettings();
            }
            using (XmlReader reader = XmlReader.Create(path, settings))
            {
                try
                {
                    reader.MoveToContent();
                }
                catch (XmlException)
                {
                    return;
                }

                while (reader.Read())
                {
                    switch (reader.NodeType)
                    {
                        case XmlNodeType.Element:
                            handler.StartElement(reader.Name);
                            break;
                        case XmlNodeType.EndElement:
                            handler.EndElement(reader.Name);
                            break;
                        case XmlNodeType.Text:
                            handler.Text(reader.Value);
                            break;
                    }
                }
            }
        }
    }
}

